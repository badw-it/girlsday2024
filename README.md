# GirlsDay2024

Hier liegen die Daten und Beispielprogramme für den Girl's Day 2024 an der BAdW.

Bitte beachtet bei der Verwendung der Beispielprogramme die Lizenzen bzw. 
Lizenzbedingungen, auf die in den Quellcodes verwiesen wird!

Bevor die Programme anderswoe hochgeladen werden, sollte auf jeden Fall geprüft 
werden, ob die Lizenzbedinungen das erlauben. Und, auch wenn das Programm nur in
abgewandelter Form irgendwo anders hochgeladen wird, sollte auf jeden Fall der
Name der Autorin oder des Autors des ursprünglichen Programms im Quellcode oder
einer beigelegten README-Datei als (Mit-)Urheberin oder Urheber genannt werden.
