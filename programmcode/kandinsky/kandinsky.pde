float x=0;
float y=0;
float speedx=5;
float speedy=5;
float theta=0;

void setup() {
  size(440,500);
  colorMode(HSB,100);
  frameRate(12);
}

void draw() {
  background(9,22,91,100);
  
  //CIRCLES ON THE TOP RIGHT
  
  stroke(69,60,50);
  
  fill(69,34,50,50);
  strokeWeight(1.5);
  circle(100,100,map(mouseX,10,440,53,110));
  
  strokeWeight(1);
  fill(12,55,45);
  circle(100,100,map(mouseX,0,440,35,90));
  
  noFill();
  strokeWeight(2.5);
  circle(100,100,map(mouseX,0,440,43,95));
  
  noFill();
  strokeWeight(2.5);
  stroke(0,0,0);
  circle(100,100,map(mouseX,0,440,85,35));
  circle(100,100,map(mouseX,0,440,94,45));
  strokeWeight(3);
  circle(100,100,map(mouseY,0,440,112,20));
  
  strokeWeight(2);
  stroke(0,88,63);
  fill(0,88,63,80);
  triangle(275,55,315,30,300,115);  //Red triangle
  
  fill(8,99,90,75);
  stroke(8,99,80);
  rect(215,167,70,60);  //yellow rect
  
  stroke(2,55,35);
  fill(2,55,35,90);
  triangle(map(mouseX,0,width,300,303),map(mouseY,0,height,90,93),map(mouseX,0,width,80,83),map(mouseY,0,height,420,423),map(mouseX,0,width,385,383),map(mouseY,0,height,250,253));  //Brown triangle
  
  fill(13,90,100,100);
  circle(165,340,35); //yellow circle
  
  stroke(55,100,35);
  fill(55,100,35,80);
  
  beginShape();   //blue trapezium
  vertex(map(mouseY,0,440,110,120),map(mouseX,0,440,320,330));
  vertex(map(mouseY,0,440,140,150),map(mouseX,0,440,313,323));
  vertex(map(mouseY,0,440,172,182),map(mouseX,0,440,378,388));
  vertex(map(mouseY,0,440,148,158),map(mouseX,0,440,414,424));
  endShape(CLOSE);
 
  // //Black rectangles
  noStroke();
  fill(55,15,19,100);

  rect(30+x,215,50,15);
  if(x>40){
    speedx=-5;
  }
  else if(x<0){
    speedx=5;
  }
  x=x+speedx;  //rect1
  
  rect(60,300+y,30,10);
  if(y>20){
    speedy=-5;
  }
  else if(y<-20){
    speedy=5;
  }
  y=y+speedy;
  
  rect(260,460,7,5);
  rect(320,430,10,10);
  
  fill(69,60,50,100); //blue square
  stroke(69,80,30);
  rect(175,410,35,35);
  
  stroke(0,0,0);
  strokeWeight(1.5);
  fill(9,22,91,100);
  ellipse(300,200,65, 65); //triangles circle
  
   //red bar
  stroke(0,88,63);
  fill(0,88,63,80);
  beginShape();  
  vertex(map(mouseX,0,440,380,399),map(mouseY,0,500,55,70)); 
  vertex(map(mouseX,0,440,390,409),map(mouseY,0,500,65,80));
  vertex(map(mouseX,0,440,245,255),map(mouseY,0,500,420,430));
  vertex(map(mouseX,0,440,242,252),map(mouseY,0,500,418,428));
  endShape(CLOSE);
 
  stroke(55,15,15);
  fill(55,15,19,100);
  triangle(75,250,365,410,330,360);  //black triangle
  
  //COLLECTION OF CIRCLES _ BOTTOM RIGHT
  
  push();
  translate(300,315);
  noStroke();
  fill(9,22,91,100);
  rotate(theta);
  ellipse(35,35,60,60);
  pop();
  
  push();
  translate(300,315);
  stroke(55,100,35);
  fill(55,100,35,80);
  rotate(theta*0.7);
  circle(45,-18,60);
  pop();
  
  fill(9,22,91,100);
  stroke(0,0,0);
  strokeWeight(2.5);
  circle(300,315,80);
  
  strokeWeight(1);
  circle(300,315,68);
  
  fill(13,90,100,40);
  circle(300,315,61);
  
  fill(8,99,90,75);
  stroke(8,99,80);
  circle(300,315,23);
  
  push();
  translate(300,315);
  stroke(0,0,0);
  noFill();
  rotate(theta*0.7);
  circle(45,-18,60);
  pop();
  
  push();
  translate(300,315);
  stroke(0,0,0);
  noFill();
  rotate(theta);
  ellipse(35,35,60, 60);
  pop();
  
  theta=theta+(mouseX+mouseY)/1500;
  
  noFill();
  stroke(0,0,0);
  strokeWeight(0.5);
  line(330,172,408,138);
  line(330,174,408,140);
  line(330,176,408,142);
  line(386,136,396,160);
  line(389,134,402,163);
  line(393,132,409,167);  //LINES
  
  arc(330,211,120,130,PI+HALF_PI,QUARTER_PI);
  arc(325,200,120,130,PI+HALF_PI,QUARTER_PI);
  
  beginShape();
  curveVertex(mouseX,mouseY);
  curveVertex(190,100);
  curveVertex(190,220);
  curveVertex(300-mouseX,300-mouseY);
  endShape();
  
  beginShape();
  curveVertex(mouseX,mouseY);
  curveVertex(180,120);
  curveVertex(180,250);
  curveVertex(400-mouseX,400-mouseY);
  endShape();
  
  strokeWeight(1.5);
  arc(360,115,60,70,PI+HALF_PI,HALF_PI);
  arc(339,171,55,65,PI+HALF_PI+QUARTER_PI,HALF_PI);
  arc(320,221,45,55,PI+HALF_PI+QUARTER_PI,HALF_PI);  //line arcs

  stroke(0,0,0);
  fill(13,90,100,100);
  ellipse(300,420,random(5,8),random(5,8)); //yellow circle
  ellipse(200,470,random(5,8),random(5,8));  
  ellipse(240,390,random(5,7),random(5,8));
  ellipse(140,450,random(5,8),random(5,8));
  
}
